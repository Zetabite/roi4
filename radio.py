#!/usr/bin/env python
from os.path import exists, realpath, dirname, isfile, basename
from os import listdir, makedirs
from shutil import copyfile
import argparse

# Initializing Standard Values
current_path = dirname(realpath(__file__)) + '/'
templatesdir = current_path + "templates/"
templategfxdir = templatesdir + "gfx/"
radioname = "myradio"
inputdir = current_path
outputdir = inputdir
volume = 1
radioupper = ""
radioreadable = ""
musicdir = ""
localedir = ""
interfacedir = ""
gfxdir = ""
radiocover = ""
songs = []
rootdir = ""
maintheme = ""
mainthemedir = ""
stationdir = ""
language = "english"


def updateVars() -> bool:
    # Options End
    # Creating and overriding values
    global radioupper, outputdir, radioreadable, rootdir
    radioupper = radioreadable.replace(' ', '')

    if outputdir.startswith(radioupper):
        outputdir = outputdir[0:-len(radioupper)]

    rootdir = outputdir + radioupper + '/'

    global musicdir
    musicdir = rootdir + "music/"

    global stationdir
    stationdir = musicdir + radioname + '/'

    global localedir
    localedir = rootdir + "localisation/"

    global interfacedir
    interfacedir = rootdir + "interface/"

    global gfxdir
    gfxdir = rootdir + "gfx/"

    global inputdir, songs
    songs = []
    for file in listdir(inputdir):
        newfile = getValidName(inputdir + file)
        if newfile != "":
            songs.append([file, newfile])
    if len(songs) == 0:
        print("No songs in folder!")
        print("Exiting...")
        return False

    # Make directories
    if not exists(stationdir):
        makedirs(stationdir)
    if not exists(localedir):
        makedirs(localedir)
    if not exists(interfacedir):
        makedirs(interfacedir)
    if not exists(gfxdir):
        makedirs(gfxdir)

    global maintheme, current_path, mainthemedir
    if maintheme != "":
        if exists(maintheme):
            maintheme = [maintheme, "maintheme.ogg"]
            songs = [maintheme] + songs
        else:
            if isfile(maintheme):
                base = basename(maintheme)
                mainthemedir = maintheme[0:-len(base)]
                maintheme = [maintheme, "maintheme.ogg"]
                songs = [maintheme] + songs
            elif isfile(current_path + maintheme):
                maintheme = current_path + maintheme
                base = basename(maintheme)
                mainthemedir = maintheme[0:-len(base)]
                maintheme = [maintheme, "maintheme.ogg"]
                songs = [maintheme] + songs
            elif isfile(inputdir + maintheme):
                maintheme = inputdir + maintheme
                base = basename(maintheme)
                mainthemedir = maintheme[0:-len(base)]
                maintheme = [maintheme, "maintheme.ogg"]
                songs = [maintheme] + songs
    else:
        print("Maintheme does not exist!")
        print("Will not set a maintheme")

    global radiocover
    if isfile(radiocover):
        radiocover = getValidName(radiocover)
    elif isfile(current_path + radiocover):
        radiocover = getValidName(current_path + radiocover)
    elif isfile(inputdir + radiocover):
        radiocover = getValidName(inputdir + radiocover)
    else:
        print("Radio cover does not exist!")
        print("Using default cover from list")
        radiocover = ""
    if radiocover == "":
        radiocover = templategfxdir + "default.dds"
    return True


def getValidName(file: str) -> str:
    newfile = ""
    base = basename(file)
    if isfile(file) and base.lower().endswith(".ogg"):
        for c in base.lower():
            if isValidLetter(c):
                newfile += c
    return newfile


def isValidLetter(char: chr) -> bool:
    numbers = [chr(48 + i) for i in range(10)]
    lowers = [chr(97 + i) for i in range(26)]
    validSymbols = lowers + numbers + ['.']
    return char in validSymbols


def fileFromTemplate(file: str, out: str):
    global templatesdir, radioname, language
    template = templatesdir
    if file.startswith("$name_"):
        template += file[6:]
        file = file.replace("$name", radioname)
    else:
        template += file
    template += ".template"
    template = open(template, 'r')
    file = out + file
    if file.endswith(".yml"):
        file = file.replace(".yml", "_%s.yml" % language)
    file = open(file, 'w')

    def descriptormod():
        global radioreadable
        file.write('name="%s"\n' % radioreadable)
        versionno = 8
        file.write('supported_version="1.%d.*"' % versionno)

    def musicasset():
        global volume, inputdir, songs
        for song in songs:
            file.write("music = {\n")
            file.write('\tname = "%s"\n' % song[1][:-4])
            file.write('\tfile = "%s/%s"\n' % (radioname, song[1]))
            file.write('\tvolume = %s\n' % str(volume))
            file.write("}\n\n")
        return

    def musicgfx():
        file.write('\t\tname = "GFX_%s_album_art"\n' % radioname)
        file.write('\t\ttexturefile = "gfx/%s_album_art.dds"\n' % radioname)
        return

    def musicgui(value: int):
        if value == 0:
            file.write('\t\tname = "%s_faceplate"\n' % radioname)
        elif value == 1:
            file.write('\t\tname = "%s_stations_entry"\n' % radioname)
        elif value == 2:
            file.write('\t\t\tquadTextureSprite = "GFX_%s_album_art"\n' % radioname)
        return

    def stationsongs():
        global radioreadable, songs
        file.write("\n # " + radioreadable + '\n')
        for song in songs:
            workname = song[1][:-4]
            readablename = song[0][:-4]
            file.write(' %s:0 "%s"\n' % (workname, readablename))
        return

    def stationtitle():
        global radioreadable
        file.write(' %s_TITLE:0 "%s"' % (radioname, radioreadable))
        return

    def station():
        global inputdir, songs
        file.write('music_station = "' + radioname + '"\n\n')
        for song in songs:
            file.write("music = {\n")
            file.write('\tsong = "' + song[1][:-4] + '"\n')
            file.write("}\n\n")
        return

    for line in template:
        trigger = line.strip()
        if "@descriptor.mod" in trigger:
            descriptormod()
        elif "@music.asset" in trigger:
            musicasset()
        elif "@music.gfx" in trigger:
            musicgfx()
        elif "@music.gui" in trigger:
            musicgui(int(trigger[10:]))
        elif "@stationsongs" in trigger:
            stationsongs()
        elif "@stationtitle" in trigger:
            stationtitle()
        elif "@songs.txt" in trigger:
            station()
        else:
            file.write(line)
    else:
        file.close()
        template.close()
        return


def main():
    global templategfxdir, templatesdir
    if current_path == templategfxdir or current_path == templategfxdir:
        print("Running in template path!")
        print("This action is not supported!")
        print("Exiting...")
        return

    global inputdir, outputdir, radioname, radiocover, radioreadable
    global volume, maintheme, language
    # Options Begin
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="path to .ogg files")
    parser.add_argument("-o", "--output", help="path to output files")
    parser.add_argument("-n", "--name", help="name of the radio station")
    parser.add_argument("-t", "--theme", help="file you want to use as theme")
    parser.add_argument("-c", "--cover", help="file you want to use as radio cover")
    parser.add_argument("-l", "--language", help="create localization for language")
    parser.add_argument("--volume", help="sets the volume")
    args = parser.parse_args()
    if args.input:
        inputdir = args.input
        if not args.input.endswith('/'):
            inputdir = inputdir + '/'
        if not exists(inputdir):
            print("Input folder does not exist! Will not proceed!")
            print("Exiting...")
            return
    if args.output:
        outputdir = args.output
        if not args.output.endswith('/'):
            outputdir = outputdir + '/'
    if args.name:
        radioreadable = args.name
        radioname = radioreadable.lower()
        radioname = radioname.replace(' ', '')
    if args.theme:
        maintheme = args.theme
    if args.cover:
        radiocover = args.cover
    if args.volume:
        volume = args.volume
    if args.language:
        language = args.language

    # Quitting in case something isnt right
    if not updateVars():
        return

    # Copying the songs to output directory
    global stationdir
    for song in songs:
        if song == maintheme:
            copyfile(mainthemedir + song[0], stationdir + song[1])
        else:
            copyfile(inputdir + song[0], stationdir + song[1])

    # Creating Localalisation files
    global localedir, musicdir
    musicymlfile = "$name_radio_l.yml"
    fileFromTemplate(musicymlfile, localedir)

    # Creating songs.txt
    songsfile = "$name_songs.txt"
    fileFromTemplate(songsfile, musicdir)

    # Creating music.asset
    assetfile = "$name_music.asset"
    fileFromTemplate(assetfile, musicdir)

    # Creating music.gfx
    global interfacedir
    gfxfile = "$name_music.gfx"
    fileFromTemplate(gfxfile, interfacedir)

    # Creating music.gui
    guifile = "$name_music.gui"
    fileFromTemplate(guifile, interfacedir)

    # Creating descriptor.mod
    global rootdir
    descriptorfile = "descriptor.mod"
    fileFromTemplate(descriptorfile, rootdir)

    # Copying dds file
    global gfxdir
    ddsfile = gfxdir + radioname + "_album_art.dds"
    copyfile(radiocover, ddsfile)


if __name__ == '__main__':
    main()
