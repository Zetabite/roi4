# Radios Of Iron 4

A python tool to help you create Hearts of Iron 4 radios.
Creates and copies files for the base radio functions.

What it does:
- Creates folder structure for the radio
- Copies over and renames songs to be used by Hoi4
- Comes with terminal options to change some parameters
- Creates base radio cover

What it doesn't do:
- Creates custom radio cover
- Adds country/alignment specific songs


How To Use:
*  Simple: Put .ogg song files in same directory as radio.py and execute.
*  Advanced: Use terminal to specify other locations if you want to.
   (see --help for further info)

Tested on Python 3. No guarantee for functionality on Python 2.